FROM golang:1.21.1-alpine3.18 AS builder
RUN apk --no-cache add ca-certificates make git && update-ca-certificates

COPY . /release-cli
WORKDIR /release-cli

RUN make build

FROM alpine:3.18

# default, overridable via `--build-arg`
ARG GLAB_VERSION=1.53.0
RUN wget https://gitlab.com/gitlab-org/cli/-/releases/v${GLAB_VERSION}/downloads/glab_${GLAB_VERSION}_linux_amd64.apk -O /tmp/glab.apk && \
    apk add --allow-untrusted /tmp/glab.apk && \
    rm /tmp/glab.apk

COPY --from=builder /release-cli/bin/release-cli /usr/local/bin/release-cli
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
