REVISION := $(shell git rev-parse --short HEAD || echo unknown)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
LAST_TAG := $(shell git describe --tags --abbrev=0)
LATEST_STABLE_TAG := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)
COMMITS := $(shell echo `git log --oneline $(LAST_TAG)..HEAD | wc -l`)
VERSION := $(shell cat VERSION)
CI_REGISTRY ?= registry.gitlab.com/gitlab-org/release-cli

# default to CGO_ENABLED=0 but can be overridden
export CGO_ENABLED := $(if $(CGO_ENABLED),$(CGO_ENABLED),0)

ifneq (v$(VERSION),$(LAST_TAG))
	VERSION := $(shell echo $(VERSION)~beta.$(COMMITS).g$(REVISION))
endif

LD_FLAGS := '-X "main.VERSION=$(VERSION)" -s -w'

_allpackages = $(shell (go list ./...))

# memoize allpackages, so that it's executed only once and only if used
allpackages = $(if $(__allpackages),,$(eval __allpackages := $$(_allpackages)))$(__allpackages)

.PHONY: setup build run clean
setup: clean
	$Q mkdir -p bin/
	$Q mkdir -p out/cover/ out/junit/
# From https://marcofranssen.nl/manage-go-tools-via-go-modules/
	@cat cmd/release-cli/tools.go | \
		grep _ | \
		awk -F'"' '{print $$2}' | \
		GOBIN=$(CURDIR)/bin xargs -tI % go install %

build: setup
	echo "VERSION: $(VERSION)"
	echo $(allpackages)
	$Q go build -ldflags $(LD_FLAGS) -o bin/$(PROJECT_NAME) ./cmd/$(PROJECT_NAME)

docker:
	$Q docker build -t ${CI_REGISTRY}:${REVISION} .

run: setup
	$Q go run cmd/$(PROJECT_NAME)/main.go

clean:
	$Q rm -rf bin/*
	$Q rm -rf out/*

BUILD_PLATFORMS ?= -os '!netbsd' -os '!openbsd'

build-binaries: setup
	# Building $(PROJECT_NAME) in version $(VERSION) for $(BUILD_PLATFORMS)
	${CURDIR}/bin/gox $(BUILD_PLATFORMS) \
		-ldflags $(LD_FLAGS) \
		-output="bin/binaries/$(PROJECT_NAME)-{{.OS}}-{{.Arch}}" \
		./cmd/$(PROJECT_NAME)
